
#Int
i = 5

#Float
f = 5.666

#String
str = "I am a String"

#List
l = [43, 67, "df", 7.7]

a = l[0]
b = l[1]

print(a, b)

# Tuple
t = (56,33,"d")

a = t[0]
b = t[1]

print(a, b)
#
# t[5] = 77 #This is an Error

# Dictionary
person = {
    "age": 6,
    "name": "Gil"
}

age = person["age"]

print(age)

# Nested
n = [
    56,
    77,
    {
        "rrr": "yyy",
        "tuple": (66,77,8.8)
    }
]

print(n[2]["tuple"][1])


i = 6.5

s = "7"

print(int(s) + (i))