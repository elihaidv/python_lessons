
#This is a comment
"""
This is a very long comment
"""

x = 3
y = 2


if x == 4:
    print("x is 3")
else:
    print("x isn't 3")

if x == 3 and y == 2:
    print("x is 3 and y is 2")

if x == 5 or y == 2:
    print("x is 5 or y is 2")

if x != 4:
    print("x isn't 3")

if x == 3:
    print("First")
    if y == 2:
        print("Second")
        if y * x == 6:
            print("canonical true")
    else:
        print("else")

if x == 5:
    print("False")
elif y == 6:
    print("False")
elif y == 3:
    print("False")
elif y == 4:
    print("False")
elif y == 5:
    print("False")
elif y == 6:
    print("False")
else:
    print("YAY")

if y == 2 and x + y == 6 or x == 3:
    print("Wooo")



